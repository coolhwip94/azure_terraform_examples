# Azure Terraform
> Simple project for me to explore and create examples of resource management and creation for Microsoft Azure utilizing Terraform.

## Examples

`azure_vm` : Simple VM created in Azure using terraform

`site_to_site_vpn` : Site to site HA vpn tunnel on the Azure side
-  https://techcommunity.microsoft.com/t5/fasttrack-for-azure/how-to-create-a-vpn-between-azure-and-aws-using-only-managed/ba-p/2281900
- shared keys are already expired in the example