# create a resource group

resource "azurerm_resource_group" "main" {
  name     = "rg-${var.session}"
  location = var.location
}

# create a vnet
resource "azurerm_virtual_network" "main" {
  name                = var.azure_vnet
  address_space       = [var.azure_vnet_address]
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
}


# create a subnet
resource "azurerm_subnet" "main" {
  name                 = var.azure_subnet
  resource_group_name  = azurerm_resource_group.main.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = [var.azure_subnet_prefix]
}


# Create public IPs
resource "azurerm_public_ip" "main" {
  name                = "pip-vpn-${var.session}"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Dynamic"
}

# public IP data
data "azurerm_public_ip" "main" {
  name                = azurerm_public_ip.main.name
  resource_group_name = azurerm_resource_group.main.name
}

# subnet for virtual network gateway
resource "azurerm_subnet" "gateway" {
  name                 = "GatewaySubnet"
  resource_group_name  = azurerm_resource_group.main.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = [var.azure_vnet_gw_subnet_address]
}

# Create virtual network gateway
resource "azurerm_virtual_network_gateway" "main" {
  name                = "vpn-${var.session}"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  type     = "Vpn"
  vpn_type = "RouteBased"

  active_active = false
  enable_bgp    = false
  sku           = "VpnGw1"

  ip_configuration {
    name                          = "vnetGatewayConfig"
    public_ip_address_id          = azurerm_public_ip.main.id
    private_ip_address_allocation = "Dynamic"
    subnet_id                     = azurerm_subnet.gateway.id
  }

}


# create local network group
resource "azurerm_local_network_gateway" "main" {
  name                = "lng-${var.session}"
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
  gateway_address     = var.primary_aws_vgw_ip
  address_space       = [var.azure_lngw_address]
}

# add connection to virtual network gateway connection
resource "azurerm_virtual_network_gateway_connection" "main" {
  name                = "connection-${var.session}"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  type                       = "IPsec"
  virtual_network_gateway_id = azurerm_virtual_network_gateway.main.id
  local_network_gateway_id   = azurerm_local_network_gateway.main.id

  shared_key = var.primary_tunnel_key

  connection_protocol = "IKEv2"
}

# create secondary local network group
resource "azurerm_local_network_gateway" "standby" {
  name                = "lng-${var.session}-standby"
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
  gateway_address     = var.standby_aws_vgw_ip
  address_space       = [var.azure_lngw_address]
}

# add secondary connection to virtual network gateway connection
resource "azurerm_virtual_network_gateway_connection" "standby" {
  name                = "connection-${var.session}-standby"
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name

  type                       = "IPsec"
  virtual_network_gateway_id = azurerm_virtual_network_gateway.main.id
  local_network_gateway_id   = azurerm_local_network_gateway.standby.id

  shared_key = var.standby_tunnel_key

  connection_protocol = "IKEv2"
}