# used for naming resources (ex : "rg-azure-aws", "vpn-azure-aws")
session = "azure-aws"
location = "East US 2"


# primary tunnel info from AWS
primary_tunnel_key = "cZGkPkt5_Dd82zmx4VH1TUmOYicym_EX"
primary_aws_vgw_ip = "3.209.75.68"

# standby tunnel info from AWS
standby_standby_tunnel_key = "I3aQgHJrOhFlrweF1JzjD5K_r407619l"
standby_aws_vgw_ip = "52.203.133.106"

# vnet on Azure side to connect to AWS
azure_vnet = "vnet-azure"
azure_vnet_address = "172.10.0.0/16"

# Default subnet for Azure vnet
azure_subnet = "subnet-01"
azure_subnet_prefix = "172.10.1.0/24"

# address for the vnet gateway
azure_vnet_gw_subnet_address = "172.10.0.0/27"

# address for local network gateway, this should be within allowed scope on AWS side
azure_lngw_address = "10.10.0.0/16"