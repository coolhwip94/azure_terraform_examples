variable "session" {
    type = string
}

variable "location" {
    type = string
    default = "East US 2"
}


variable "primary_tunnel_key" {
    type = string
}

variable "primary_aws_vgw_ip" {
    type = string
}

variable "standby_tunnel_key" {
    type = string
}

variable "standby_aws_vgw_ip" {
    type = string
}



variable "azure_vnet" {
    type = string
}

variable "azure_vnet_address" {
    type = string
}

variable "azure_vnet_gw_subnet_address" {
    type = string
}

variable "azure_subnet" {
    type = string
}

variable "azure_subnet_prefix" {
    type = string
}

variable "azure_lngw_address" {
    type = string
}